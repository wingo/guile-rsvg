;; About the package
(define *name* "Guile-RSVG")
(define *description* "SVG graphics with Scheme and librsvg")
(define *version* "2.18.0")
(define *updated* "27 September 2007")
(define *authors*
  '(("Andy Wingo" . "wingo at pobox.com")))

;; Copying the documentation
(define *copyright-holder* "Andy Wingo")
(define *years* '(2007))
(define *permissions*
  "Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU General Public License, Version 3 or any
later version published by the Free Software Foundation.")

;; Texinfo info
(define *texinfo-basename* "guile-rsvg")
(define *texinfo-category* "The Algorithmic Language Scheme")
(define *extra-texinfo-menu-entries*
  '(("Function Index")))
(define *texinfo-epilogue*
  `((node (% (name "Function Index")))
    (unnumbered "Function Index")
    (printindex (% (type "fn")))))

;; HTML foo
(define *html-relative-root-path* "../../../")
(define *extra-html-entry-files*
  '()) ;("scripts.texi" "org-to-pdf-presentation"
     ;"Make PDF presentations from Org Mode files")))

;; The modules to document
(define *modules*
  '(((rsvg)
     "foo bar bas")))

(define *module-sources* '())

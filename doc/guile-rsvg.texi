\input texinfo   @c -*-texinfo-*-
@c %**start of header
@setfilename guile-rsvg.info
@settitle Guile-RSVG
@c %**end of header

@copying 
This manual is for Guile-RSVG (version 2.18.0, updated 27 September
2007)

Copyright 2007 Andy Wingo

@quotation 
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU General Public License, Version 3 or any
later version published by the Free Software Foundation.

@end quotation

@end copying

@dircategory The Algorithmic Language Scheme
@direntry 
* Guile-RSVG: (guile-rsvg.info).  SVG graphics with Scheme and librsvg.
@end direntry

@titlepage 
@title Guile-RSVG
@subtitle version 2.18.0, updated 27 September 2007
@author Andy Wingo (@email{wingo at pobox.com})
@page 
@vskip 0pt plus 1filll
@insertcopying 
@end titlepage

@ifnottex 
@node Top
@top Guile-RSVG
@insertcopying 
@menu 
* rsvg::                 foo bar bas

* Function Index::       
@end menu

@end ifnottex

@iftex 
@shortcontents 
@end iftex

@node rsvg
@chapter (rsvg)
@section Overview
@verbatim 
 This is the RSVG wrapper for Guile.

 See the rsvg documentation for more details.
@end verbatim

@section Usage
@anchor{rsvg rsvg-handle-close}@defun rsvg-handle-close _
@end defun

@anchor{rsvg rsvg-handle-get-base-uri}@defun rsvg-handle-get-base-uri _
@end defun

@anchor{rsvg rsvg-handle-get-desc}@defun rsvg-handle-get-desc _
@end defun

@anchor{rsvg rsvg-handle-get-dimensions}@defun rsvg-handle-get-dimensions _
@end defun

@anchor{rsvg rsvg-handle-get-metadata}@defun rsvg-handle-get-metadata _
@end defun

@anchor{rsvg rsvg-handle-get-title}@defun rsvg-handle-get-title _
@end defun

@anchor{rsvg rsvg-handle-new}@defun rsvg-handle-new 
@end defun

@anchor{rsvg rsvg-handle-new-from-data}@defun rsvg-handle-new-from-data _
@end defun

@anchor{rsvg rsvg-handle-new-from-file}@defun rsvg-handle-new-from-file _
@end defun

@anchor{rsvg rsvg-handle-render-cairo}@defun rsvg-handle-render-cairo _ _
@end defun

@anchor{rsvg rsvg-handle-render-cairo-sub}@defun rsvg-handle-render-cairo-sub _ _ _
@end defun

@anchor{rsvg rsvg-handle-set-base-uri}@defun rsvg-handle-set-base-uri _ _
@end defun

@anchor{rsvg rsvg-handle-set-dpi}@defun rsvg-handle-set-dpi _ _
@end defun

@anchor{rsvg rsvg-handle-set-dpi-x-y}@defun rsvg-handle-set-dpi-x-y _ _ _
@end defun

@anchor{rsvg rsvg-handle-write}@defun rsvg-handle-write _ _
@end defun

@anchor{rsvg rsvg-set-default-dpi-x-y}@defun rsvg-set-default-dpi-x-y _ _
@end defun

@anchor{rsvg rsvg-version}@defun rsvg-version 
@end defun

@node Function Index
@unnumbered Function Index
@printindex fn
@bye

define(GUILE_RSVG_CONFIGURE_COPYRIGHT,[[

Copyright (C) 2007, 2011, 2014 Andy Wingo <wingo@pobox.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program.  If not, see
<http://www.gnu.org/licenses/>.

]])

AC_INIT(guile-rsvg, 2.18.1)
AC_COPYRIGHT(GUILE_RSVG_CONFIGURE_COPYRIGHT)
AC_CONFIG_SRCDIR(HACKING)
AC_CONFIG_AUX_DIR([build-aux])
AM_CONFIG_HEADER(config.h)
AM_INIT_AUTOMAKE([-Wall -Wno-portability serial-tests])

AC_DISABLE_STATIC

AC_ISC_POSIX
AC_PROG_CC
AC_STDC_HEADERS
AC_PROG_LIBTOOL

WARN_CFLAGS=-Wall
AC_ARG_ENABLE([Werror], AC_HELP_STRING([--enable-Werror],[Stop the build on errors]),
        [WARN_CFLAGS="-Wall -Werror"], [])
AC_SUBST(WARN_CFLAGS)

GUILE_PKG([3.0 2.2 2.0 1.8])

PKG_CHECK_MODULES(GUILE_CAIRO, guile-cairo >= 1.4.0)
AC_SUBST(GUILE_CAIRO_LIBS)
AC_SUBST(GUILE_CAIRO_CFLAGS)

PKG_CHECK_MODULES(RSVG, librsvg-2.0)
AC_SUBST(RSVG_LIBS)
AC_SUBST(RSVG_CFLAGS)

AC_CONFIG_FILES(
Makefile
guile-rsvg.pc
guile-rsvg/Makefile
rsvg/Makefile
examples/Makefile
tests/Makefile
tests/unit-tests/Makefile
doc/Makefile
)
AC_OUTPUT

/* guile-rsvg
 * Copyright (C) 2007, 2014 Andy Wingo <wingo at pobox dot com>
 *
 * guile-rsvg.c: RSVG for Guile
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *                                                                  
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *                                                                  
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, contact:
 *
 * Free Software Foundation, Inc.     Voice:  +1-617-542-5942
 * 51 Franklin St, Fifth Floor        Fax:    +1-617-542-2652
 * Boston, MA  02110-1301,  USA       gnu@gnu.org
 */

#include <libguile.h>

#include <guile-cairo.h>
#include <librsvg/rsvg.h>

static scm_t_bits scm_tc16_rsvg_t;

static SCM
scm_take_rsvg_handle (RsvgHandle *h)
{
    SCM shandle;

    if (!h)
        return SCM_BOOL_F;

    SCM_NEWSMOB (shandle, scm_tc16_rsvg_t, h);

    return shandle;
}

static RsvgHandle*
scm_to_rsvg_handle (SCM scm)
{
    scm_assert_smob_type (scm_tc16_rsvg_t, scm);
    return (RsvgHandle*)SCM_SMOB_DATA (scm);
}

static size_t
scm_rsvg_free (SCM smob)
{
    RsvgHandle *h = (RsvgHandle*)SCM_SMOB_DATA (smob);

    SCM_SET_SMOB_DATA (smob, NULL);
    g_object_unref (h);

    return 0;
}

static void
scm_c_check_rsvg_error (GError *error, const char *subr)
{
    if (error) {
        g_error_free (error);
        scm_error (scm_from_locale_symbol ("rsvg-error"),
                   subr,
                   "",
                   SCM_EOL,
                   SCM_EOL);
    }
}

SCM_DEFINE_PUBLIC (scm_rsvg_version, "rsvg-version", 0, 0, 0,
	    (void),
	    "Retrieves the version of the rsvg library.")
#define FUNC_NAME s_scm_rsvg_version
{
    return scm_list_3 (scm_from_int (LIBRSVG_MAJOR_VERSION),
                       scm_from_int (LIBRSVG_MINOR_VERSION),
                       scm_from_int (LIBRSVG_MICRO_VERSION));
}
#undef FUNC_NAME

SCM_DEFINE_PUBLIC (scm_rsvg_set_default_dpi_x_y, "rsvg-set-default-dpi-x-y", 2, 0, 0,
                   (SCM dpi_x, SCM dpi_y),
	    "")
{
    rsvg_set_default_dpi_x_y (scm_to_double (dpi_x), scm_to_double (dpi_y));
    return SCM_UNSPECIFIED;
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_set_dpi, "rsvg-handle-set-dpi", 2, 0, 0,
                   (SCM handle, SCM dpi),
	    "")
{
    rsvg_handle_set_dpi (scm_to_rsvg_handle (handle), scm_to_double (dpi));
    return SCM_UNSPECIFIED;
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_set_dpi_x_y, "rsvg-handle-set-dpi-x-y", 3, 0, 0,
                   (SCM handle, SCM dpi_x, SCM dpi_y),
	    "")
{
    rsvg_handle_set_dpi_x_y (scm_to_rsvg_handle (handle), scm_to_double (dpi_x),
                             scm_to_double (dpi_y));
    return SCM_UNSPECIFIED;
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_new, "rsvg-handle-new", 0, 0, 0,
                   (void),
	    "")
{
    return scm_take_rsvg_handle (rsvg_handle_new ());    
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_write, "rsvg-handle-write", 2, 0, 0,
                   (SCM handle, SCM str),
	    "")
{
    GError *error = NULL;
    char *buf;
    gboolean ret;

    scm_dynwind_begin (0);
    buf = scm_to_locale_string (str);
    scm_dynwind_free (buf);

    ret = rsvg_handle_write (scm_to_rsvg_handle (handle), (guchar*)buf,
                             scm_c_string_length (str), &error);
    scm_c_check_rsvg_error (error, s_scm_rsvg_handle_write);

    scm_dynwind_end ();

    return scm_from_bool (ret);
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_close, "rsvg-handle-close", 1, 0, 0,
                   (SCM handle),
	    "")
{
    GError *error = NULL;
    gboolean ret;
    ret = rsvg_handle_close (scm_to_rsvg_handle (handle), &error);
    scm_c_check_rsvg_error (error, s_scm_rsvg_handle_close);
    return scm_from_bool (ret);
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_get_base_uri, "rsvg-handle-get-base-uri", 1, 0, 0,
                   (SCM handle),
	    "")
{
    return scm_from_locale_string
        (rsvg_handle_get_base_uri (scm_to_rsvg_handle (handle)));
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_set_base_uri, "rsvg-handle-set-base-uri", 2, 0, 0,
                   (SCM handle, SCM suri),
	    "")
{
     char *uri;

    scm_dynwind_begin (0);
    uri = scm_to_locale_string (suri);
    scm_dynwind_free (uri);

    rsvg_handle_set_base_uri (scm_to_rsvg_handle (handle), uri);

    scm_dynwind_end ();
    return SCM_UNSPECIFIED;
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_get_dimensions, "rsvg-handle-get-dimensions", 1, 0, 0,
                   (SCM handle),
	    "")
{
    RsvgDimensionData dim;

    rsvg_handle_get_dimensions (scm_to_rsvg_handle (handle), &dim);
    return scm_values (scm_list_4 (scm_from_int (dim.width),
                                   scm_from_int (dim.height),
                                   scm_from_double (dim.em),
                                   scm_from_double (dim.ex)));
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_get_title, "rsvg-handle-get-title", 1, 0, 0,
                   (SCM handle),
	    "")
{
    return scm_from_locale_string
        (rsvg_handle_get_title (scm_to_rsvg_handle (handle)));
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_get_desc, "rsvg-handle-get-desc", 1, 0, 0,
                   (SCM handle),
	    "")
{
    return scm_from_locale_string
        (rsvg_handle_get_desc (scm_to_rsvg_handle (handle)));
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_get_metadata, "rsvg-handle-get-metadata", 1, 0, 0,
                   (SCM handle),
	    "")
{
    return scm_from_locale_string
        (rsvg_handle_get_metadata (scm_to_rsvg_handle (handle)));
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_new_from_data, "rsvg-handle-new-from-data", 1, 0, 0,
                   (SCM str),
	    "")
{
    GError *error = NULL;
    char *buf;
    RsvgHandle *ret;

    scm_dynwind_begin (0);
    buf = scm_to_locale_string (str);
    scm_dynwind_free (buf);

    ret = rsvg_handle_new_from_data ((const guchar*)buf, scm_c_string_length (str),
                                     &error);
    scm_c_check_rsvg_error (error, s_scm_rsvg_handle_new_from_data);

    scm_dynwind_end ();

    return scm_take_rsvg_handle (ret);
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_new_from_file, "rsvg-handle-new-from-file", 1, 0, 0,
                   (SCM sfilename),
	    "")
{
    GError *error = NULL;
    char *filename;
    RsvgHandle *ret;

    scm_dynwind_begin (0);
    filename = scm_to_locale_string (sfilename);
    scm_dynwind_free (filename);

    ret = rsvg_handle_new_from_file (filename, &error);
    scm_c_check_rsvg_error (error, s_scm_rsvg_handle_new_from_file);

    scm_dynwind_end ();

    return scm_take_rsvg_handle (ret);
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_render_cairo, "rsvg-handle-render-cairo", 2, 0, 0,
                   (SCM handle, SCM cr),
	    "")
{
    rsvg_handle_render_cairo (scm_to_rsvg_handle (handle),
                              scm_to_cairo (cr));
    scm_c_check_cairo_status (cairo_status (scm_to_cairo (cr)),
                              s_scm_rsvg_handle_render_cairo);
    return SCM_UNSPECIFIED;    
}

SCM_DEFINE_PUBLIC (scm_rsvg_handle_render_cairo_sub, "rsvg-handle-render-cairo-sub", 3, 0, 0,
                   (SCM handle, SCM cr, SCM sid),
	    "")
{
    char *id;

    scm_dynwind_begin (0);
    id = scm_to_locale_string (sid);
    scm_dynwind_free (id);

    rsvg_handle_render_cairo_sub (scm_to_rsvg_handle (handle),
                                  scm_to_cairo (cr), id);
    scm_c_check_cairo_status (cairo_status (scm_to_cairo (cr)),
                              s_scm_rsvg_handle_render_cairo_sub);

    scm_dynwind_end ();

    return SCM_UNSPECIFIED;    
}

void
scm_init_rsvg (void)
{
    static int initialized = 0;

    if (initialized)
        return;

#ifndef SCM_MAGIC_SNARFER
#include "guile-rsvg.x"
#endif

    scm_tc16_rsvg_t = scm_make_smob_type ("rsvg-handle", 0);
    scm_set_smob_free (scm_tc16_rsvg_t, scm_rsvg_free);

    rsvg_init ();    

    initialized = 1;
}

#!/bin/bash
# -*- scheme -*-
exec guile --debug -e main -s "$0" "$@"
!#

(use-modules (cairo)
             (rsvg)
             (srfi srfi-11)
             (ice-9 format))

(define (svg->pdf in out)
  (let*-values (((handle) (rsvg-handle-new-from-file in))
                ((width height em ex) (rsvg-handle-get-dimensions handle))
                ((surf) (cairo-pdf-surface-create width height out))
                ((ctx) (cairo-create surf)))
               (rsvg-handle-render-cairo handle ctx)
               (cairo-show-page ctx)
               (cairo-surface-finish surf)))

(define (main args)
  (or (= (length args) 3)
      (begin
        (format (current-error-port) "usage: ~A SVG-FILE OUTPUT-PDF-FILE\n"
                (car args))
        (exit 1)))
  (apply svg->pdf (cdr args)))
